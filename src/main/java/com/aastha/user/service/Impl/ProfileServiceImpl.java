package com.aastha.user.service.Impl;

import java.time.Instant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aastha.user.entity.User;
import com.aastha.user.json.ProfileRequest;
import com.aastha.user.json.ProfileResponse;
import com.aastha.user.repository.IUserRepository;
import com.aastha.user.service.ProfileService;

@Service
public class ProfileServiceImpl implements ProfileService {
	
	@Autowired
	private IUserRepository iUserRepository;

	@Override
	public ProfileResponse addProfile(ProfileRequest request) {
		User user = new User();
		user.setEmail(request.getEmail());
		user.setPhoneNumber(request.getPhoneNumber());
		user.setCreatedAt(Instant.now());
		user.setUpdatedAt(Instant.now());
		user = iUserRepository.save(user);
		
		ProfileResponse response = new ProfileResponse();
		response.setId(user.getId());
		
		return response;
		
	}

	@Override
	public ProfileResponse updateProfile(ProfileRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProfileResponse getProfile(Long profileId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteProfile(Long profileId) {
		// TODO Auto-generated method stub
		
	}

}
