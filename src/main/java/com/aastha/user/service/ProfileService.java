package com.aastha.user.service;

import com.aastha.user.json.ProfileRequest;
import com.aastha.user.json.ProfileResponse;

public interface ProfileService {
	public ProfileResponse addProfile(ProfileRequest request);
	public ProfileResponse updateProfile(ProfileRequest request);
	public ProfileResponse getProfile(Long profileId);
	public void deleteProfile(Long profileId);

}
