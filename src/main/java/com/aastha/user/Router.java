package com.aastha.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.aastha.user.api.UserApi;
import com.aastha.user.json.ProfileRequest;
import com.aastha.user.json.ProfileResponse;
import com.aastha.user.service.ProfileService;

@RestController
public class Router implements UserApi {
	
	@Autowired ProfileService profileService;

	@Override
	public void addProfile(ProfileRequest request) {
		profileService.addProfile(request);
	}

	@Override
	public ProfileResponse updateProfile(ProfileRequest request) {
		return profileService.addProfile(request);
	}

	@Override
	public ProfileResponse getProfile(Long profileId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteProfile(Long profileId) {
		// TODO Auto-generated method stub
		
	}

}
