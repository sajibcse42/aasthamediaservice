package com.aastha.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages= {"com.aastha.user"})
public class AasthaUserServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AasthaUserServiceApplication.class, args);
	}

}

